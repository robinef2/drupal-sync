<?php

declare(strict_types=1);

namespace Ef2\DrupalSync\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents';

    protected $guarded = ['id'];

    protected $casts = [
        'data' => 'array',
    ];
}

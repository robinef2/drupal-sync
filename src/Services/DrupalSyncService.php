<?php

declare(strict_types=1);

namespace Ef2\DrupalSync\Services;

use Ef2\DrupalSync\Models\Content;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DrupalSyncService
{
    public function synchronize(): bool
    {
        $response = Http::acceptJson()->get(config('drupal_sync.endpoint'), [
            'token' => JWT::encode([], config('drupal_sync.jwt_secret')),
        ]);

        if ($response->failed()) {
            Log::error('Something went wrong:' . $response->toException()->getMessage());

            return false;
        }

        $response->collect()->map(function ($content) {
            foreach ($content['data'] as $language => $data) {
                Content::query()->updateOrCreate(
                    [
                        'identifier' => $data['identifier']
                    ],
                    [
                        'url'     => $data['url'] ?? null,
                        'lang'    => $language,
                        'type'    => $content['type'],
                        'subtype' => $content['subtype'],
                        'data'    => $data['data']
                    ]
                );
            }
        });

        return true;
    }
}

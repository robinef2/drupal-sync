<?php

declare(strict_types=1);

namespace Ef2\DrupalSync;

use Illuminate\Support\ServiceProvider;

class DrupalSyncServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/drupal_sync.php',
            'drupal_sync'
        );
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/drupal_sync.php' => config_path('drupal_sync.php'),
            ], 'config');
        }

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
    }
}


<?php

declare(strict_types=1);

namespace Ef2\DrupalSync\Http\Controllers;

use Illuminate\Routing\Controller;
use Ef2\DrupalSync\Services\DrupalSyncService;
use Illuminate\Http\Request;

class DrupalSyncController extends Controller
{
    public function webhook(Request $request, DrupalSyncService $drupalSyncService)
    {
        $drupalSyncService->synchronize();
    }
}

<?php

return [
    'endpoint'   => env('DRUPAL_SYNC_ENDPOINT'),
    'jwt_secret' => env('DRUPAL_SYNC_JWT_SECRET'),
];

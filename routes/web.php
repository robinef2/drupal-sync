<?php

declare(strict_types=1);

use Ef2\DrupalSync\Http\Controllers\DrupalSyncController;
use Illuminate\Support\Facades\Route;

Route::post('/drupal-sync/webhook', [DrupalSyncController::class, 'webhook']);
